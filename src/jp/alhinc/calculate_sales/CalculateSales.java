package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LIST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// 支店定義ファイルのフォーマット
	private static final String BRANCH_FILE_REGEX = "^[0-9]+$";

	// 商品定義ファイルのフォーマット
	private static final String PRODUCT_FILE_REGEX = "^[0-9a-zA-Z]{8}$";

	// 売上ファイル名の正規表現
	private static final String RCD_FILE_NAME_REGEX = "^[0-9]{8}\\.rcd$";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String BRANCH_FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String BRANCH_FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String SERIAL_NUMBER_ERROR = "売上ファイル名が連番になっていません";
	private static final String SALES_AMOUNT_ERROR = "合計金額が10桁を超えました";
	private static final String PRODUCT_FILE_NOT_EXIST = "商品定義ファイルが存在しません";
	private static final String PRODUCT_FILE_INVALID_FORMAT = "商品定義ファイルのフォーマットが不正です";



	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> productNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> productSales = new HashMap<>();

		//コマンドライン引数が渡されているかの確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}


		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, BRANCH_FILE_REGEX, BRANCH_FILE_NOT_EXIST, BRANCH_FILE_INVALID_FORMAT)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LIST, productNames, productSales, PRODUCT_FILE_REGEX, PRODUCT_FILE_NOT_EXIST, PRODUCT_FILE_INVALID_FORMAT)) {
			return;
		}


		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)

		//ディレクトリ内のすべてのファイルを取得
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {

			//ファイル名が正規表現regexに該当し、フォルダではなくファイルであるものをrcdFilesに格納する
			if(files[i].isFile() && files[i].getName().matches(RCD_FILE_NAME_REGEX)) {
				rcdFiles.add(files[i]);
			}

		}

		//売上ファイルが連番かどうかを確認
		for(int i = 0; i < rcdFiles.size() - 1; i++) {

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println(SERIAL_NUMBER_ERROR);
				return;
			}

		}


		//rcdFilesに格納されているファイルの内容を読み込む
		BufferedReader br = null;

		for(int i = 0; i < rcdFiles.size(); i++) {

			try {

				String fileName = rcdFiles.get(i).getName();

				File file = rcdFiles.get(i);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				//対象のファイルの支店コードと売上金額を保持するList
				List<String> rcdFileContents = new ArrayList<String>();

				// 一行ずつ読み込み、内容をrcdFilesContentsに格納
				String line;
				while((line = br.readLine()) != null) {
					rcdFileContents.add(line);
				}

				//rcdFilesContents.get(0):支店コード、rcdFilesContents.get(1):商品コード、rcdFilesContents.get(2):売上金額
				String branchCode = rcdFileContents.get(0);
				String productCode = rcdFileContents.get(1);
				String sale = rcdFileContents.get(2);

				//売上ファイルのフォーマットを確認する
				if(rcdFileContents.size() != 3) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}

				//売上ファイルの支店コードが支店定義ファイルに存在するか確認する
				if(!branchNames.containsKey(branchCode)) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				//売上ファイルの商品コードが商品定義ファイルに存在するか確認する
				if(!productNames.containsKey(productCode)) {
					System.out.println(fileName + "の商品コードが不正です");
					return;
				}

				//売上金額が数字なのかを確認する
				if(!sale.matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}


				//売上金額をlong型に変換
				long fileSale = Long.parseLong(sale);

				//branchSalesから取得した売上金額にrcdFilesで読み込んだ売上金額を加算
				Long branchSalesAmount = branchSales.get(branchCode) + fileSale;

				//productSalesから取得した売上金額にrcdFilesで読み込んだ売上金額を加算
				Long productSalesAmount = productSales.get(productCode) + fileSale;

				//売上金額の合計が10桁を超えたか確認する
				if(branchSalesAmount >= 10000000000L || productSalesAmount >= 10000000000L){
					System.out.println(SALES_AMOUNT_ERROR);
					return;
				}

				//branchSalesAmountをbranchSalesに格納
				branchSales.put(branchCode, branchSalesAmount);

				//productSalesAmountをproductSalesに格納
				productSales.put(productCode, productSalesAmount);

			}catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			}finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		//商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, productNames, productSales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String regex, String fileErrorMessage, String formatErrorMessage) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//ファイルが存在しないかのチェック
			if(!file.exists()) {
				System.out.println(fileErrorMessage);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			// 一行ずつ読み込む
			String line;

			while((line = br.readLine()) != null) {

				String[] item = line.split(",");

				//フォーマットを確認する
				if(item.length != 2 || !item[0].matches(regex)) {
					System.out.println(formatErrorMessage);
					return false;
				}

					names.put(item[0], item[1]);
					sales.put(item[0], 0L);

			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		//支店別集計ファイルの作成
		File file = new File(path, fileName);

		BufferedWriter bw = null;

		try {

			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//支店別集計ファイルへの出力内容を記述
			for(String key : names.keySet()) {
				bw.write(key + ',' + names.get(key) + ',' + sales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}